from mongoDBOperations import MongoDBManagement
from datetime import datetime
import pandas as pd
import schedule
import requests
import pymongo
import certifi
import time


# Database credentials
username = 'Zahoor'
password = 'aXnTvIYxLAwBAbxq'
db_name = 'YachtClub-Scraper'
collection_name = 'YachtClub'

# Define scraper function
def scraper(username, password, db_name, collection_name):
    try:
	# datetime object containing current date and time
        now = datetime.now()
	
        # Get data from an API
        url = 'https://niftyprice.herokuapp.com/collections/:boredapeyachtclub'
        response = requests.request("GET", url)

        json_data = response.json()['message']
        df = pd.DataFrame(response.json()["message"])[['date','floorpurchaseprice','totalforsale','volume','owners']]

        mongoClient = MongoDBManagement(username=username, password=password)

        if mongoClient.isDatabasePresent(db_name=db_name):
            try:
                sort_df = df.sort_values(by=['date'], ascending=False, ignore_index=True)
                last_date = mongoClient.findDateofLastRecord(db_name=db_name, collection_name=collection_name)
                _index_no = sort_df.index[sort_df['date'] == last_date[0]].to_list()
                get_new_data = sort_df.iloc[0:_index_no[0]]
                get_new_json_data = get_new_data.to_dict(orient='records')
                if get_new_json_data:
                    mongoClient.insertRecords(db_name=db_name, collection_name=collection_name, records=get_new_json_data)
                    print("Data Stored at: ",now)
                else:
                    print("Empty data: ", now)
                    pass
            except Exception as e:
                raise Exception("(app.py) - Something went wrong on storing latest data.\n" + str(e))
        else:
            try:
                mongoClient.createDatabase(db_name=db_name)
                mongoClient.createCollection(collection_name=collection_name, db_name=db_name)
                mongoClient.insertRecords(db_name=db_name, collection_name=collection_name, records=json_data)
                print("Data has been stored successfully: ",now)
            except Exception as e:
                raise Exception("(app.py) - Something went wrong on storing data.\n" + str(e))

    except Exception as e:
        raise Exception("(app.py) - Something went wrong on scrapping data from an API.\n" + str(e))



# Task scheduling
# After every 5mins scraper() is called.
schedule.every(5).minutes.do(lambda: scraper(username, password, db_name, collection_name))

# Loop so that the scheduling task
# keeps on running all time.
while True:
	# Checks whether a scheduled task
	# is pending to run or not
	schedule.run_pending()
	time.sleep(1)
